FROM kalilinux/kali-rolling
RUN apt update
RUN apt install -y dirb \
    curl \
    sqlmap \
    hashcat \
    seclists \
    hash-identifier
