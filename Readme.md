# Juice Shop Training

## What's going on here?
    
In this training session we're going to take a look at some common tools and techniques generally used in basic web-based attacks. This will include Directory busting, login brute forcing, automated SQL injections, and some hash cracking thrown in for fun. This lab is meant to be simple, so we won't be going very in depth on each topic. 

To do this we'll be using Docker Compose to confige a mini lab so that everything that we do is contained. The only things you will need installed are:

* [Docker Compose](https://docs.docker.com/compose/install/)
* [Burp Suite Community](https://portswigger.net/burp/communitydownload)

## Lab setup

The first thing we need to do is get our lab environment set up. We'll be running two containers, one for the target running [Juice Shop](https://github.com/bkimminich/juice-shop) and the other acting as our attcker running a slimmed down version of Kali linux.

**NOTE:** To keep this image small, we have only installed the tools that we'll be using in this lab. If you wish to explore further you may need to install additional tools or take a look at the [Kali Metapackages](https://tools.kali.org/kali-metapackages). You do not need to install anything whatsoever to go through these excercises.

As mentioned, we'll be using Docker Compose to make building this lab easy. To do this we'll need the configuration files. To get those, clone this repository and enter it like so:
```
git clone https://gitlab.com/jshodd/juice-shop-training.git
cd juice-shop-training
```

With that done, let's get our containers built and running. The first step is going **disconnecting from the corporate vpn**. It shouldn't cause an issue, but because we're deploying an intentionally vulnerable web application to our machine it's best practice to isolate it however possible. 

Then we move on to building our kali image using Docker Compose. This will take several minutes depending on network speeds to install the various tools we'll be using. 

```
docker-compose build
```

After this completes we are ready to start our vulnerable application, to do so we'll run the following command:

```
docker-compose up -d
```

To confirm that Juice Shop is running in the contianer, open your web browser and navigate to http://127.0.0.1:3000 and you should see the Juice Shop home page. The next thing we want to do is start our attacker container. To do so run:

```
docker-compose run attacker
```

This will put you into a shell within the Kali linux container. Let's confirm that our containers can talk to each other by sending a get request from our attacker to the target container:

**NOTE:** Within the container network, you can reference containers by using container names. With this being said any requests coming from the attacker container will be directed to the `target` hostname.

```
curl http://target:3000
```

The last thing we will want to set up is our HTTP proxy. In this lab we will be using Burp Suit, but there are plenty of other alternatives out there ([ZAP](https://www.zaproxy.org/) is a popular option). After starting Burp we'll create a temporary project and press `Start Burp` using the Burp defaults. After this the main Burp window should be open. From here we'll navigate to the `Proxy` tab and click `Open Browser`. This will open a chromium browser that is already configured to use Burp as a proxy for all traffic. For now, we'll turn intercept off under the `Proxy` tab in Burp.

## Let's get this thing started

To save time, these exercises are going to be pretty quick and not very in-depth. The idea is to introduce the ideas and tactics behind common attacks, not necessarily to turn you into a full blown hacker :). Feel free to dig further into the various topics or other areas in Juice Shop that we won't get to today!

### Directory Busting

In short terms, Directory Busting is simply enumerating sub-directories and is generally the first stop when enumerating a web application. This is just a specific type of Fuzzing where we go through a wordlist and make a requests with various sub-directory names. The ones that don't exist will return errors while the ones that do won't. This is handy for finding possibly "hidden" parts of a website that aren't intended for users interact with or for finding directories that were made available due to a misconfiguration in the server. You can use any fuzzing tool to do this, but in this example we'll be using `dirb`, which is made for this purpose. To run our scan we will execute the following command:

```
dirb http://target:3000 /usr/share/seclists/Discovery/Web-Content/common.txt
```

`dirb` will then send a request for each line in our wordlist, which is just a collection of common sub-directory names, and report back on which directories returned favorable responses. This shows us several interesting locations that are most likely sensitive (`/ftp` certainly sounds juicey).

### Password Brute Forcing

Finding "hidden" directories can be fun and all, but how about we try our hand at brute forcing a login? If we take a look at the homepage for Juice Shop we see an `Account` button in the top right corner. This just screams "Authentication" and we wouldn't be attackers if we didn't investigate. After clicking on the button we're brought to a login screen that is askign for an email and a password. Emails are tough becasue we aren't sure what domain is used for the users of the site. Let's put this on hold and do some recon. 

Often times we can find hints to the username/email schema for a website by looking at behaviour by real users if we can. In this case, we can click on some of the products shown on the homepage and take a look at some of the `Reviews`. Clicking from product to product we see `bender@juice-sh.op`, `ubogin@juice-sh.op`, and last (but certainly not least) `admin@juice-sh.op`. Now we know that any emails will use the `XXXX@juice-sh.op` format. We can use this information to possibly fuzz unknown usernames while brute forcing the login, but let's stick with a user that we know exists so that we are only brute forcing a singe variable. 

Navigate back to the login page via tha `Account` button. Becuase we know it exists, lets enter `admin@juice-sh.op` as the email and `PASSWORD` as the password and click `Log in`. Unsurprisingly we get an error stating we have either the email or password incorrect. But let's see if we can't guess that password with the help of some technology!

Remember that Burp Proxy? Under the `Proxy` tab in burp click `HTTP history` and take a look at the recent requests (we're looking for anything related to the login we just attempted). If we scroll down to the bottom we will see that there was a `POST` request sent to `/rest/user/login`. Clicking that line will pull up the request that was sent and the response given back to us. Now we could manually copy and paste from a list of common passwords, but let's use Burp for that. Right click anywhere in the `Request` or `Response` windows and click `send to intruder`. 

This will send a copy of the request to Burp's Intruder tool. Now this is severely rate limited on the community version of Burp, but we are only using it as an example. There are *many* tools for brute forcing logins that are incredibly fast. With that done, click the `Intruder` tab in burp, and then on the `Positions` sub-tab. Here we will be selecting where in the request Burp will be injecting the password guesses. To start lets click `Clear` on the right hand side. We'll then highlight `PASSWORD` (not including the double quotes) and click `Add` on the right. Now Burp knows where to substitute it's guesses.

The next step is to tell Burp what password list to use. For this usecase I have provided a small wordlist that shouldn't take too long to go through to act as an example. It is incredibly easy to obtain password lists as long as 15 million passwords to run through. Click the `Payloads` sub-tab to continue. Under `Payload Options` we will click `Load...` and then select the file `brute_force_wordlist.txt` included in this repo. When this is done we can click `Start attack` in the top left to begin. 

You will then see all of the attempts that Burp has made so far and the HTTP code sent back in response. After some time we will see an attemp that returned an HTTP 200 code, success!. It looking at the request that was sent it seems that `admin123` is the password we are looking for. Returning to the login page we can login using this new password.

### Manual Blind SQL Injections

I'm sure you've heard of SQL injections, but today we're focusing on a specific sub-type of injection which is a blind SQL injection. This essentially means that we can manipulate the query being executed on the database, but we aren't able to return any data. Well, we aren't able to return any data _directly_. There are generally two different types of bling SQL injections, which are timed and boolean based injections. Today we'll be working with a Boolean based injection. 

Apart from login pages, anything related to search is always a good target when looking for a SQL injection. Lucky for us theres a handy magnifying glass button that pulls up a search bar on the man page of the aplication. Let's just do a sample search for `FUZZ` to try it out. It won't return any data, but it _will_ generate some traffic for us to examine. 

Going back to Burp, under the `Proxy`/`HTTP history` tab we can take a look at the requests sent. After some searching we find a request to `/rest/products/search?q=`. Let's click on this request, then in the `Request` or `Response` window, right click and select `Send to Repeater`. This will allow up to modify and repeatedly send this request, which is very handy for examining how an application handles different types of input.

**NOTE:** This section may be hard to follow along with, but in general you will only be modifying this part of the request:
```
GET /rest/products/search?q=<HERE>
```

Let's first see what a valid request looks like. First we'll set the value of `q` to `test` like so:

```
GET /rest/products/search?q=test
```

Then click the `Send` button above the request. This will send the request and will recieve a response from the web server. To see the response we click `Response`. Looking here we see that we have some results returned! So now we know that a valid response looks like:

```
HTTP/1.1 200 OK
Access-Control-Allow-Origin: *
X-Content-Type-Options: nosniff
X-Frame-Options: SAMEORIGIN
Feature-Policy: payment 'self'
Content-Type: application/json; charset=utf-8
Content-Length: 517
ETag: W/"205-HnS2VVRNJOdMQ/Le9MAE4jhE30Y"
Vary: Accept-Encoding
Date: Thu, 09 Sep 2021 01:35:49 GMT
Connection: close

{"status":"success","data":[{"id":9,"name":"OWASP SSL Advanced Forensic Tool (O-Saft)","description":"<snip...>","price":0.01,"deluxePrice":0.01,"image":"orange_juice.jpg","createdAt":"2021-09-09 00:54:19.195 +00:00","updatedAt":"2021-09-09 00:54:19.195 +00:00","deletedAt":null}]}
```

So now let's see if we can't break it with some SQL special characters. First we'll try a single quote:

```
GET /rest/products/search?q='
```

Only it looks like the web server was still happy with that request. Let's try adding another special character `;`:

```
GET /rest/products/search?q=';
```

Bingo! The server is sending back a SQLITE error and is showing us what query it attempted to make. The `;` character in many SQL languages is going to terminate the query, which means we terminated the query in the middle of the intended query, leading to some syntax errors. Here is what the database tried to execute:

```
SELECT * FROM Products WHERE ((name LIKE '%';%' OR description LIKE '%';%') AND deletedAt IS NULL) ORDER BY name
```

Now that we know we can modify the query, let's work on making SQLITE happy. To do this we want to create a query without any syntax errors. To do this we'll close out the two parentheses before terminating the query with `;`. Let's try it out with the following request:

```
GET /rest/products/search?q='));
```

Interesting, it looks like the server responded will all possible objects? This is because of how the query was modified. In the end, this is what was executed by SQLITE:

```
SELECT * FROM Products WHERE ((name LIKE '%'));
```

Meaning we searched for any object with a `name` like `%` (which is a wildcard). Let's make that part of the query return FALSE instead of True. We'll change the string that `name` is compared to into something we know won't be found, then use Boolean statements to experiment. First, let's send a query that returns FALSE:

**NOTE:** `+` is a space in URL encoding

```
GET /rest/products/search?q=nonexistant'+OR+'1'+=+'2'));
```

The response doesn't return an error, but we also don't get any results, now let's change the Boolean statement to resolve to TRUE:

```
GET /rest/products/search?q=nonexistant'+OR+'1'+=+'1'));
```

After sending that, we get a list of all products. This is the basis of a Boolean based blind SQL injection! Now we can use any boolean based query and know if it resolved to TRUE or FALSE based on the response from the web server. This can be used to compare the length of table names with numbers until we get it right, then compare the first letter of the first table to every letter until we get it right... and you can see where this is going. But that's enough of this manual work. Let's step up our game and automate this vulnerability.

## Steping our Blind SQL Injection Game up

So we've got our hands dirty and determined that we have a blind sql injection on our hands, but we really want to know what's in there! It'll take forever to manually compare every character in the database. This is where a tool called sqlmap comes in. First we'll use sqlmap to confirm that the vulnerability exists.

**NOTE:** Don't worry too much about the flags, that isn't the point of the exercise

**SUPER NOTE:** It probably doesn't need to be said, but for the love of all that is good please don't run this against anything live.

We'll start by verifying our hard work with this command from our attacker container:

```
sqlmap http://target:3000/rest/products/search?q= -p 'q' --level 3 
```

Sqlmap confirms our suspision and tells us that a boolean-based blind sql injection is present. But we already knew that! let's get into some more juicy information. Let's start by getting the tables present in the database:

```
sqlmap http://target:3000/rest/products/search?q= -p 'q' --level 3 --threads 8 --tables
```

With this we'll see sqlmap go through and identify each character with boolean based logic! We see that there is a table named `Users`, let's see what columns are present there:

```
sqlmap http://target:3000/rest/products/search?q= -p 'q' --level 3 --threads 8 -T Users --columns
```

Now that we have full access to the database, anything really is possible. But for the sake of the exercise let's pull a list of all of the password hashes using SQLmap:

```
sqlmap http://target:3000/rest/products/search?q= -p 'q' --level 3 --threads 8 -T Users -C password --dump
```

After sqlmap dumps all of the password hashes, it's even nice enough to ask us if we'd like it to attempt to crack those passwords! selecting yes, then using the default worlist cracks three of the weaker passwords. The remaining could be attacked using more robust cracking methods that we won't get into here.
